<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
	protected $fillable = ['announcements_owner','user_id','channel_id','start_time', 'end_time','description','note','bitrate_conparation'];

	protected $dates = ['deleted_at'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }


    public function channel()
    {
    	return $this->belongsTo(Channel::class);
    }
}
