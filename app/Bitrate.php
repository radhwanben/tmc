<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitrate extends Model
{
		protected $fillable = ['channel_id','frequency','bitrate_value', 'default_bitrate','id_user'];


    public function channel()
    {
    	return $this->belongsTo(Channel::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
