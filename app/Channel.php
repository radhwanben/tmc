<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
	protected $fillable = ['name','frequency','polarity', 'satelite','bandwith','start_time_monotoring','bitrate_conparation','nationalite','is_active'];


    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function bitrate()
    {
    	return $this->hasMany(Bitrate::class);
    }

    public function problem()
    {
    	return $this->belongsTo(Problem::class);
    }

    public function announcement()
    {
        return $this->hasMany(Announcement::class);
    }

    public function Contactinformation()
    {
        return $this->hasMany(Channel::class);
    }

    public function teleport()
    {
        return $this->belongsTo(Teleport::class);
    }

		public function provider()
		{
				return $this->belongsTo(Provider::class);
		}

}
