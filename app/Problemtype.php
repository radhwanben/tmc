<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Problemtype extends Model
{
	protected $fillable = ['name'];
    public function problem()
    {
    	return $this->hasMany(Problem::class);
    }

    public function channel()
    {
    	return $this->hasMany(Channel::class);

    }

    
}
