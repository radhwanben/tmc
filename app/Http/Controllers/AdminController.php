<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Hash;
use App\Channel;
use App\User;
use Auth;
use App\Problem;
use App\Bitrate;
use App\Announcement;
use App\Problemtype;
use App\Contactinformation;
use App\Teleport;
use App\Provider;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{

    public function __construct()
    {
    $this->middleware('auth');
    $this->middleware('admin',['except' => ['selectAjax']]);
    }



    public function index()
    {

    $data = DB::table('users')
       ->select(
        DB::raw('role as role'),
        DB::raw('count(*) as number'))
       ->groupBy('role')
       ->get();
     $array[] = ['Role', 'Number'];
     foreach($data as $key => $value)
     {
      $array[++$key] = [$value->role, $value->number];
     }
    $channel = DB::table('channels')
       ->select(
        DB::raw('satelite as satelite'),
        DB::raw('count(*) as number'))
       ->groupBy('satelite')
       ->get();
     $array2[] = ['satelite', 'Number'];
     foreach($channel as $key => $value)
     {
      $array2[++$key] = [$value->satelite, $value->number];
     }
    $channels=Channel::all();
    //dd($channels);
    $users=User::whereIn('role',array('admin','employee'))->get();
    $announcements=Announcement::orderBy('created_at','desc')->paginate(20);

    return view('admin.index',compact('users','channels','announcements'))
    ->with('role', json_encode($array))
    ->with('satelite' ,json_encode($array2));

    }

/*category Problems Management*/



    public function selectAjax(Request $request)
    {
        if($request->ajax()){

            $states = DB::table('problemsubcategorys')
            ->where('id_category',$request->id_category)
            ->pluck("name","id")
            ->all();

            $data = view('admin.ajax-select',compact('states'))->render();
            return response()->json(['options'=>$data]);
        }
    }


/* User Management */

    public function GetUserData()
    {
        $users=User::all();

        return view('admin.user.index',compact('users'));
    }

    public function CreateUser()
    {
    	return view ('admin.createUser');
    }


    public function StoreUsers(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255' ,
            'email' => 'required|string|email|max:255|unique:users' ,
            'password' => 'required|string|min:6',
            'role' => 'required|in:admin,employee,customer', //validate role input
        ]);

        $name = $request['name'];
        $email = $request['email'];
        $password = bcrypt($request['password']);
        $role = $request['role'];

        $user = new User();
        $user->name=$name;
        $user->email=$email;
        $user->password=$password;
        $user->role=$role;
        $user->save();

        return redirect()->route('Dashbord');
    }

    public function EditUsers($id)
    {
        $user = User::find($id);
        return view('admin.user.EditUser', compact('user'));
    }

    public function UserUpdate(Request $request ,$id)
    {
        $user = User::find($id);
        $name=$request->input('name');
        $email=$request->input('email');
        $password=$request->input('password');
        $role=$request->input('role');

        $user->name=$name;
        $user->email=$email;
        $user->password=Hash::make($password);
        $user->role=$role;
        $user->update();

        return redirect()->route('Dashbord');
    }


    public function UsersDelete($id)
    {
        $user =User::find($id);
        $user->delete();
        return redirect()->route('Dashbord');
    }



/* Channel Management */

    public function channelsIndex()
    {
        $channel = DB::table('channels')
       ->select(
        DB::raw('satelite as satelite'),
        DB::raw('count(*) as number'))
       ->groupBy('satelite')
       ->get();
     $array2[] = ['satelite', 'Number'];
     foreach($channel as $key => $value)
     {
      $array2[++$key] = [$value->satelite, $value->number];
     }
        $teleports=Teleport::all();
        $providers =Provider::all();
        $channels=Channel::orderBy('created_at','desc')->paginate(20);
        return view('admin.chanels.index',compact('channels','teleports','providers'))->with('satelite' ,json_encode($array2));

    }

    public function channelShow($id)
    {
        $channel= Channel::find($id);
        $teleports=Teleport::all();
        $providers=Provider::all();
        $contacts=Contactinformation::all();
        $announcements=Announcement::all();
        return view(
            'admin.chanels.channelShow' ,
            compact('channel','teleports','providers','contacts' ,'announcements')
        );
    }


    public function channelsCreate()
    {
        $providers =Provider::all();
        $teleports=Teleport::all();
        return view('admin.chanels.createchannel' ,compact('teleports' ,'providers'));
    }

    public function channelStore(Request $request)
    {
       //dd($request);
        if($request->hasFile('logo')){
            $request->validate([
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            ]);
        $logo=$request->file('logo');
        $logo->move(public_path('src/photos/'.$logo->getClientOriginalName()));
        }

        $channels= new Channel();
        $channels->name = $request->input('name');
        $channels->frequency = $request->input('frequency');
        $channels->satelite = $request->input('satelite');
        $channels->polarity  = $request->input('polarity');
        $channels->bandwith  = $request->input('bandwith');
        $channels->start_time_monotoring = $request->input('start_time_monotoring');
        $channels->bitrate_conparation = $request->input('bitrate_conparation');
        $channels->nationalite = $request->input('nationalite');
        $channels->logo = $request->file('logo');
        $channels->id_user = Auth::user()->id;
        $channels->save();
        return redirect()->route('GetChannelsData');
    }

    public function channelsEdit($id)
    {
        $channel = Channel::find($id);
        $teleports=Teleport::all();
        $providers =Provider::all();


        return view('admin.chanels.editchannel', compact('channel','teleports','providers'));
    }

    public function ChannelUpdate(Request $request , $id)
    {
       // dd($request->all());
        $channel= Channel::find($id);
        if($request->hasFile('logo')){
            $request->validate([
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            ]);
        $logo=$request->file('logo');
        $logo->move('storage',$logo->getClientOriginalName());
        }

        $channel= Channel::find($id);
        $channel->name=$request->input('name');
        $channel->frequency=$request->input('frequency');
        $channel->polarity=$request->input('polarity');
        $channel->satelite=$request->input('satelite');
        $channel->bandwith=$request->input('bandwith');
        $channel->start_time_monotoring =$request->input('start_time_monotoring');
        $channel->bitrate_conparation=$request->input('bitrate_conparation');
        $channel->nationalite=$request->input('nationalite');
        if($request->hasFile('logo')){
        $channel->logo=$request->file('logo')->getClientOriginalName();
        }
        $channel->teleport_id=$request->input('teleport_id');
        $channel->provider_id=$request->input('provider_id');
        $channel->is_active=$request->input('is_active');
        $channel->id_user=Auth::user()->id;
        $channel->save();

        return redirect()->route('GetChannelsData');
    }

    public function ChannelDelete($id)
    {
        $channel=Channel::find($id);
        $channel->delete();
        return redirect()->route('GetChannelsData');
    }

/* Problems Management */

    public function ProblemsIndex()
    {

        $problems = Problem::orderBy('created_at','desc')->paginate(20);
        return view('admin.problems.problemsindex',compact('problems'));
    }

    public function ProblemsCreate(Request $request)
    {
        $channels=Channel::all();
        $categorys=DB::table('problemscategorys')->pluck("name","id")->all();

        return view('admin.problems.ProblemsCreate' ,compact('channels','categorys'));
    }

    public function ProblemsSave(Request $request)
    {

        $channel = $request->input('channel');
        $id_category = $request->input('id_category');
        $id_details = $request->input('id_details');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');
        $reason = $request->input('reason');
        $action = $request->input('action');
        $description = $request->input('description');
        $note = $request->input('note');
        $user_id = Auth::user()->id;

        $problem = Problem::Create(array(
            'channel_id'=>$channel,
            'problem_id'=>$id_category,
            'problemsub_id'=>$id_details,
            'start_time'=>$start_time,
            'end_time'=>$end_time,
            'reason'=>$reason,
            'what_you_do'=>$action,
            'description'=>$description,
            'note'=>$note,
            'user_id'=>$user_id
        ));
        return redirect()->route('GetProblemsData');

    }

    public function problemsEdit($id)
    {
        $problem = Problem::find($id);
         $channels=Channel::all();
         $categorys=DB::table('problemscategorys')->pluck("name","id")->all();
        return view('admin.problems.problemsEdit',compact('problem','categorys','channels'));

    }

    public function ProblemUpdate(Request $request , $id)
    {
        $problem =Problem::find($id);
        $problem->channel_id = $request->input('channel');
        $problem->problem_id = $request->input('id_category');
        $problem->problemsub_id = $request->input('id_details');
        $problem->start_time = $request->input('start_time');
        $problem->end_time = $request->input('end_time');
        $problem->reason = $request->input('reason');
        $problem->what_you_do = $request->input('action');
        $problem->description = $request->input('description');
        $problem->note = $request->input('note');
        $problem->user_id = Auth::user()->id;
        $problem->save();


        return redirect()->route('GetProblemsData');

    }


    public function ProblemDelete($id)
    {
        $problem=Problem::destroy($id);

        return redirect()->route('GetProblemsData');
    }

/* Announcement Management */

    public function AnnouncementShow()
    {
        $announcements=Announcement::orderBy('created_at','desc')->paginate(20);
        $users=User::whereIn('role',array('admin','employee'))->get();
        $channels=Channel::all();
        return view('admin.announcements.index',compact('announcements','users','channels'));
    }

    public function AnnouncementShowCompleted()
    {
        $announcements=Announcement::where('confirmed' , 1)->get();
        return view('admin.announcements.indexcompleted', compact('announcements'));
    }


    public function AnnouncementCreate(Request $request)
    {
        $announcements_owner = $request->input('announcements_owner');
        $user_id = Auth::user()->id;
        $channel_id = $request->input('channel_id');
        $note = $request->input('note');
        $description = $request->input('description');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');

        $announcement = Announcement::Create(array(
            'announcements_owner'=>$announcements_owner,
            'user_id'=>$user_id,
            'channel_id'=>$channel_id,
            'note'=>$note,
            'description'=>$description,
            'start_time'=>$start_time,
            'end_time'=>$end_time,
        ));
        return redirect()->route('Dashbord');
    }



    public function AnnouncementsEdit($id)
    {
        $announcement=Announcement::find($id);
        $users=User::whereIn('role',array('admin','employee'))->get();
        $channels=Channel::all();
        return view('admin.announcements.AnnouncementsEdit',compact('announcement','users','channels'));
    }

    public function AnnouncementUpdate(Request $request,$id)
    {
        $announcement=Announcement::find($id);
        $announcement->announcements_owner = $request->input('announcements_owner');
        $announcement->user_id = Auth::user()->id;
        $announcement->channel_id = $request->input('channel_id');
        $announcement->note = $request->input('note');
        $announcement->description = $request->input('description');
        $announcement->start_time = $request->input('start_time');
        $announcement->end_time = $request->input('end_time');
        $announcement->confirmed = $request->input('confirmed');
        //dd($announcement);
        $announcement->save();
        return redirect()->route('AnnouncementShow');
    }

    public function AnnouncemetDelete($id)
    {

        $announcement=Announcement::destroy($id);

        return redirect()->route('AnnouncementShow');


    }

/* Bitrate Management */

    public function BitrateShow()
    {
        $bitrates=Bitrate::orderBy('created_at','desc')->paginate(3);
        //dd($bitrates);
        $users = User::all();
        $channels=Channel::all();
        return view('admin.bitrates.index', compact('bitrates','channels','users'));
    }

    public function BitrateCreate()
    {

        $channels=Channel::all();
        return view('admin.bitrates.BitrateCreate', compact('channels'));
    }



    public function BitrateSave(Request $request)
    {
        $channel_id=$request->input('channel_id');
        $id_user=Auth::user()->id;
        $frequency=Channel::where('id',$channel_id)->pluck("frequency")->first();
        $bitrate_value=$request->input('bitrate_value');
        $default_bitrate=Channel::where('id',$channel_id)->pluck("bitrate_conparation")->first();

        $bitrates = Bitrate::Create(array(
            'channel_id'=>$channel_id,
            'frequency'=>$frequency,
            'bitrate_value'=>$bitrate_value,
            'default_bitrate'=>$default_bitrate,
            'id_user'=>$id_user,

        ));
        return redirect()->back();

    }


    public function BitrateEdit($id)
    {
        $bitrate=Bitrate::find($id);
        $channels=Channel::all();
        return view('admin.bitrates.BitrateEdit',compact('bitrate','channels'));
    }



    public function BitrateUpdate(Request $request ,$id)
    {
        $bitrate= Bitrate::find($id);
        $bitrate->channel_id=$request->input('channel_id');
        $bitrate->id_user=Auth::user()->id;
        $bitrate->frequency=Channel::where('id', $request->input('channel_id'))->pluck("frequency")->first();
        $bitrate->bitrate_value=$request->input('bitrate_value');
        $bitrate->default_bitrate=Channel::where('id',$request->input('channel_id'))->pluck("bitrate_conparation")->first();
        $bitrate->save();

        return redirect()->route('BitrateShow');

    }

    public function ProblemsTypeShow()
    {
        $types=DB::table('problemscategorys')->get();
        $subtypes=DB::table('problemsubcategorys')->get();
        //dd($subtypes);
        return view('admin.problems.typeproblem',compact('types','subtypes'));
    }


    public function ProblemsTypeCreate(Request $request)
    {
        $name =$request->input('name');
        DB::table('problemscategorys')->insert(
        ['name' => $name]
    );
             return back();
    }

    public function ProblemsTypeEdit($id)
    {
        $types=DB::table('problemscategorys')->where('id', $id)->first();
        //dd($types);
        return view('admin.problems.typeproblemedit',compact('types'));
    }

    public function ProblemstypesUpdate(Request $request ,$id)
    {
        $types=DB::table('problemscategorys')->where('id', $id)->update(['name' => $request->input('name')]);
        return redirect()->route('ProblemsTypeShow');
    }

    public function ProblemstypesDelete($id)
    {
        $types=DB::table('problemscategorys')->where('id' ,$id)->delete();
        return redirect()->view('admin.problems.typeproblem');
    }




    public function ProblemsSubTypeCreate(Request $request)
    {

    $name =$request->input('name');
    $type =$request->input('type');
    DB::table('problemsubcategorys')->insert(
        ['name' => $name,
        'id_category' => $type
]
    );
             return back();
    }

    public function ProblemsubTypesEdit($id)
    {
       $subtypes=DB::table('problemsubcategorys')->where('id', $id)->first();
        //dd($types);
        return view('admin.problems.subtypeproblemedit',compact('subtypes'));
    }



    public function ProblemsubTypesUpdate(Request $request , $id)
    {
        $types=DB::table('problemsubcategorys')->where('id', $id)->update(['name' => $request->input('name')]);
        return redirect()->route('ProblemsTypeShow');
    }





    public function ContactinformationShow()
    {
        $contacts=Contactinformation::all();
        return view('admin.clients.index',compact('contacts'));
    }


    public function ContactinformationCreate()
    {
        $channels= Channel::all();
        return view('admin.clients.create',compact('channels'));
    }


    public function ContactinformationSave(Request $request)
    {
       $contacts= Contactinformation::forceCreate(Request()->except(['_token']));
        return redirect()->route('ContactinformationShow');
    }


    public function TeleportShow()
    {
        $teleports=Teleport::all();
        return view('admin.teleports.index',compact('teleports'));
    }

    public function TeleportCreate()
    {
        return view('admin.teleports.create');
    }

    public function TeleportSave(Request $request)
    {
        $name=$request->input('name');
        $location=$request->input('location');
        $contact=$request->input('contact');

        $teleports = Teleport::Create(array(
            'name'=>$name,
            'location'=>$location,
            'contact'=>$contact,
        ));
        return redirect()->route('TeleportShow');

    }

    public function TeleportEdit($id)
    {
        $teleports=Teleport::find($id);
        return view('admin.teleports.edit',compact('teleports'));
    }


    public function TeleportUpdate(Request $request , $id)
    {
        $teleports=Teleport::find($id);
        $teleports->name=$request->input('name');
        $teleports->location=$request->input('location');
        $teleports->contact=$request->input('contact');
        $teleports->save();

        return redirect()->route('TeleportShow');

    }

    public function TeleportDelete($id)
    {
        $teleports=Teleport::destroy($id);

        return redirect()->route('TeleportShow');
    }


    public function providershow()
    {
        $providers= Provider::all();
        return view('admin.providers.index', compact('providers'));
    }


    public function ProviderCreate()
    {
        return view('admin.providers.create');
    }

    public function providersave(Request $request)
    {
        $name=$request->input('name');
        $location=$request->input('location');
        $contact=$request->input('contact');

        $teleports = Provider::Create(array(
            'name'=>$name,
            'location'=>$location,
            'contact'=>$contact,
        ));
        return redirect()->route('providershow');

    }


    public function ProviderEdit($id)
    {
        $providers=Provider::find($id);
        return view('admin.providers.edit' , compact('providers'));
    }


    public function ProviderUpdate(Request $request,$id)
    {
        $providers=Provider::find($id);
        $providers->name=$request->input('name');
        $providers->location=$request->input('location');
        $providers->contact=$request->input('contact');
        $providers->save();
        return redirect()->route('providershow');

    }


    public function ProviderDelete($id)
    {
        $providers=Provider::destroy($id);

        return redirect()->route('providershow');
    }

    public function export()
    {
        return Excel::download(new UsersExport(), 'problems.xlsx');
    }


}
