<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use DB;
use App\Channel;
use App\User;
use Auth;
use App\Problem;
use App\Bitrate;
use App\Announcement;
use App\Teleport;
use App\Provider;
use App\Contactinformation;

class EmployeeController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');    
    }



	public function Dashbord()
	{
		$channels=Channel::all();
    	$users=User::whereIn('role',array('admin','employee'))->get();
        $announcements=Announcement::orderBy('created_at','desc')->paginate(2);
		return view('employee.index',compact('users','channels','announcements'));
	}
    

    public function EmployeeAnnouncemetCreate(Request $request)
    {
    	$announcements_owner = $request->input('announcements_owner');
        $user_id = Auth::user()->id;
        $channel_id = $request->input('channel_id');
        $note = $request->input('note');
        $description = $request->input('description');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');
        
        $announcement = Announcement::Create(array(
            'announcements_owner'=>$announcements_owner,
            'user_id'=>$user_id,
            'channel_id'=>$channel_id,
            'note'=>$note,
            'description'=>$description,
            'start_time'=>$start_time,
            'end_time'=>$end_time,
        ));
        return redirect()->route('EmployeeDashbord');
    }

    public function channelsIndex()
    {
    	$channels=Channel::all();
        $teleports=Teleport::all();
        $providers=Provider::all();

        return view('employee.chanels.index',compact('channels','teleports','providers'));
    }

        public function EmpchannelsCreate()
    {
        $teleports=Teleport::all();
        return view('employee.chanels.createchannel' ,compact('teleports'));
    }


     public function EmpchannelStore(Request $request)
    {
        //dd($request->all());
        $channels=Channel::forceCreate(Request()->except(['_token']));
        return redirect()->route('EmployeeGetChannelsData');
    }
        public function empchannelsEdit($id)
    {
        $channel = Channel::find($id);
        $teleports=Teleport::all();
        $providers=Provider::all();
        return view('employee.chanels.editchannel', compact('channel' , 'teleports','providers'));
    }


    public function empChannelUpdate(Request $request , $id)
    {
        $channel= Channel::find($id);
        if($request->hasFile('logo')){
            $request->validate([
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            ]);
        $logo=$request->file('logo');
        $logo->move('storage',$logo->getClientOriginalName());
        }

        $channel= Channel::find($id);
        $channel->name=$request->input('name');
        $channel->frequency=$request->input('frequency');
        $channel->polarity=$request->input('polarity');
        $channel->satelite=$request->input('satelite');
        $channel->bandwith=$request->input('bandwith');
        $channel->start_time_monotoring =$request->input('start_time_monotoring');
        $channel->bitrate_conparation=$request->input('bitrate_conparation');
        $channel->nationalite=$request->input('nationalite');
        $channel->teleport_id=$request->input('teleport_id');
        $channel->id_user=Auth::user()->id;
        $channel->save();

        return redirect()->route('EmployeeGetChannelsData');
    }


        public function empChannelDelete($id)
    {
        $channel=Channel::find($id);
        $channel->is_active =0;
        $channel->save();
        return redirect()->route('EmployeeGetChannelsData');
    }


        public function empchannelsShow($id)
    {
        $channel= Channel::find($id);
        $teleports=Teleport::all();
        $providers=Provider::all();
        $contacts=Contactinformation::all();
        $announcements=Announcement::all();
        return view(
            'employee.chanels.channelShow' ,
            compact('channel','teleports','providers','contacts' ,'announcements')
        );  
    }


    public function empBitrateShow()
    {
        $fromDate = new Carbon('last week'); 
        $toDate = new Carbon('now'); 
        $bitrates=Bitrate::whereBetween('created_at', array($fromDate->toDateTimeString(), $toDate->toDateTimeString()) )->get();
        $channels=Channel::all();
        return view('employee.bitrates.index', compact('bitrates','channels'));
    }

    public function empBitrateCreate()
    {
        
        $channels=Channel::all();
        return view('employee.bitrates.BitrateCreate', compact('channels'));
    }



    public function empBitrateSave(Request $request)
    {
        $channel_id=$request->input('channel_id');
        $id_user=Auth::user()->id;
        $frequency=Channel::where('id',$channel_id)->pluck("frequency")->first();
        $bitrate_value=$request->input('bitrate_value');
        $default_bitrate=Channel::where('id',$channel_id)->pluck("bitrate_conparation")->first();

        $bitrates = Bitrate::Create(array(
            'channel_id'=>$channel_id,
            'frequency'=>$frequency,
            'bitrate_value'=>$bitrate_value,
            'default_bitrate'=>$default_bitrate,
            'id_user'=>$id_user,

        ));
        return redirect()->back();

    }


    public function empBitrateEdit($id)
    {
        $bitrate=Bitrate::find($id);
        $channels=Channel::all();
        return view('employee.bitrates.BitrateEdit',compact('bitrate','channels'));
    }



    public function empBitrateUpdate(Request $request ,$id)
    {
        $bitrate= Bitrate::find($id);
        $bitrate->channel_id=$request->input('channel_id');
        $bitrate->id_user=Auth::user()->id;
        $bitrate->frequency=Channel::where('id', $request->input('channel_id'))->pluck("frequency")->first();
        $bitrate->bitrate_value=$request->input('bitrate_value');
        $bitrate->default_bitrate=Channel::where('id',$request->input('channel_id'))->pluck("bitrate_conparation")->first();
        $bitrate->save();

        return redirect()->route('empBitrateShow');

    }



    /* Problems Management */
    
    public function empProblemsIndex()
    {

        $problems = Problem::all();
        return view('employee.problems.problemsindex',compact('problems'));
    }

    public function empProblemsCreate(Request $request)
    {
        $channels=Channel::all();
        $categorys=DB::table('problemscategorys')->pluck("name","id")->all();

        return view('employee.problems.ProblemsCreate' ,compact('channels','categorys'));
    }

    public function empProblemsSave(Request $request)
    {
        
        $channel = $request->input('channel');
        $id_category = $request->input('id_category');
        $id_details = $request->input('id_details');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');
        $reason = $request->input('reason');
        $action = $request->input('action');
        $description = $request->input('description');
        $note = $request->input('note');
        $user_id = Auth::user()->id;
        
        $problem = Problem::Create(array(
            'channel_id'=>$channel,
            'problem_id'=>$id_category,
            'problemsub_id'=>$id_details,
            'start_time'=>$start_time,
            'end_time'=>$end_time,
            'reason'=>$reason,
            'what_you_do'=>$action,
            'description'=>$description,
            'note'=>$note,
            'user_id'=>$user_id
        ));
        return redirect()->route('GetProblemsData');
    
    }

    public function empproblemsEdit($id)
    {
        $problem = Problem::find($id);
         $channels=Channel::all();
         $categorys=DB::table('problemscategorys')->pluck("name","id")->all();
        return view('employee.problems.problemsEdit',compact('problem','categorys','channels'));

    }

    public function empProblemUpdate(Request $request , $id)
    {
        $problem =Problem::find($id);
        $problem->channel_id = $request->input('channel');
        $problem->problem_id = $request->input('id_category');
        $problem->problemsub_id = $request->input('id_details');
        $problem->start_time = $request->input('start_time');
        $problem->end_time = $request->input('end_time');
        $problem->reason = $request->input('reason');
        $problem->what_you_do = $request->input('action');
        $problem->description = $request->input('description');
        $problem->note = $request->input('note');
        $problem->user_id = Auth::user()->id;
        $problem->save();


        return redirect()->route('GetProblemsData');    

    }


    public function empProblemDelete($id)
    {
        $problem=Problem::destroy($id);

        return redirect()->route('GetProblemsData');
    }

        public function empselectAjax(Request $request)
    {
        if($request->ajax()){

            $states = DB::table('problemsubcategorys')
            ->where('id_category',$request->id_category)
            ->pluck("name","id")
            ->all();
            
            $data = view('employee.ajax-select',compact('states'))->render();
            //dd($data);
            return response()->json(['options'=>$data]);
        }
    }


    public function empAnnouncementsEdit($id)
    {
        $announcement=Announcement::find($id);
        $users=User::whereIn('role',array('admin','employee'))->get();
        $channels=Channel::all();
        return view('employee.announcements.AnnouncementsEdit',compact('announcement','users','channels'));
    }


    public function empAnnouncementUpdate(Request $request,$id)
    {
        $announcement=Announcement::find($id);
        $announcement->announcements_owner = $request->input('announcements_owner');
        $announcement->user_id = Auth::user()->id;
        $announcement->channel_id = $request->input('channel_id');
        $announcement->note = $request->input('note');
        $announcement->description = $request->input('description');
        $announcement->start_time = $request->input('start_time');
        $announcement->end_time = $request->input('end_time');
        $announcement->confirmed = $request->input('confirmed');
        //dd($announcement);
        $announcement->save();
        return redirect()->route('EmployeeDashbord');
    }


    public function empAnnouncementShow()
    {
        $announcements=Announcement::orderBy('created_at','desc')->paginate(20);
        $users=User::whereIn('role',array('admin','employee'))->get();
        $channels=Channel::all();
        return view('employee.announcements.index',compact('announcements','users','channels'));
    }



    public function empAnnouncementShowCompleted()
    {
        $announcements=Announcement::where('confirmed' , 1)->get();
        return view('employee.announcements.indexcompleted', compact('announcements'));            
    }

}

