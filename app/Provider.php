<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
		protected $fillable = ['name', 'location', 'contact'];
		protected $dates = ['deleted_at'];

		public function channel()
		{
			$this->hasMany(Channel::class);
		}

}
