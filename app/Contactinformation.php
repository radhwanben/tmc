<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactinformation extends Model
{
        protected $fillable = [
        'channel_id', 'name', 'email','phone','other','job_title','notes','teleport_id','language'
    	];

    	public function channel()
    	{
    		return $this->belongsTo(Channel::class);
    	}
}
