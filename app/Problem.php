<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Problem extends Model
{

	use SoftDeletes;
	
		protected $fillable = ['channel_id', 'problem_id', 'problemsub_id', 'start_time', 'end_time', 'reason', 'what_you_do', 'description', 'note', 'user_id'];

		protected $dates = ['deleted_at'];

		public function channel()
		{
			return $this->belongsTo(Channel::class);
		}

		public function user()
		{
			return $this->belongsTo(User::class);
		}		
}
