<?php

namespace App\Exports;

use App\Problem;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Problem::all();
    }

    public function headings(): array
    {
        return [
            'id',
            'channel_id',
            'problem_id',
            'problemsub_id',
            'start_time',
            'end_time',
            'reason',
            'what_you_do',
            'description',
            'note',
            'user_id',
        ];
    }
}
