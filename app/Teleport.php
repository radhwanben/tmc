<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teleport extends Model
{
    protected $fillable = ['name', 'location', 'contact'];
	protected $dates = ['deleted_at'];

    public function channel()
    {
    	$this->hasMany(Channel::class);
    }

}
