<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','last_login_at','last_login_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function channel()
   {
    return $this->hasMany(Channel::class);
   }

   public function bitrate()
   {
    return $this->hasMany(Bitrate::class);
   }

   public function problem()
   {
    return $this->hasMany(Problem::class);
   }

   public function announcement()
   {
    return $this->hasMany(Announcement::class);
   }


}
