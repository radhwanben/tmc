<div class="col-md-2">
    <div class="card">
        <div class="card-header">MENU</div>

        <div class="nav flex-column nav-pills">
            <a class="nav-link " href="{{route('Dashbord')}}">Home</a>
            <a class="nav-link" href="{{route('UsersLists')}}">Users</a>
            <a class="nav-link" href="{{route('GetChannelsData')}}">Channels</a>
            <a class="nav-link" href="{{route('GetProblemsData')}}">Problems</a>
            <a class="nav-link" href="{{route('BitrateShow')}}">Bitrates</a>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Settings
        </a>
                <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                    <a class="dropdown-item " href="{{route('AnnouncementShow')}}">Announcement</a>
                    <a class="dropdown-item" href="{{route('ProblemsTypeShow')}}">Problems Type</a>
                    <a class="dropdown-item" href="{{route('ContactinformationShow')}}">Contact Informations</a>
                    <a class="dropdown-item" href="{{route('TeleportShow')}}">Teleports</a>
                    <a class="dropdown-item" href="{{route('providershow')}}">Providers</a>
                </div>
            </li>
        </div>
     </div>
</div>