
    <div class="form-group">
      <label>Select Type:</label>
      {!! Form::select('id_category',[''=>'--- Select Type ---']+$categorys,null,['class'=>'form-control']) !!}
    </div>


    <div class="form-group">
      <label>Select details:</label>
      {!! Form::select('id_details',[''=>'--- Select details ---'],null,['class'=>'form-control']) !!}
    </div>



<script type="text/javascript">
 function loadCategory(e){
      //console.log(e);
      var id_category = $(this).val();
      var token = $("input[name='_token']").val();
      $.ajax({
          url: "<?php echo route('empselectAjax') ?>",
          method: 'POST',
          data: {id_category:id_category, _token:token},
          success: function(data) {
            $("select[name='id_details'").html('');
            $("select[name='id_details'").html(data.options);
          }
      });
  }
  $(document).ready(function(){
    $("select[name='id_category']")[0].addEventListener('change', loadCategory);
  })
</script>
