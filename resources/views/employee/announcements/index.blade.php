@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.employee-navbar')

    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                ANNOUNCEMENTS
                <div class="text-right">
                  <a class="btn btn-sm btn-success" href="{{route('empAnnouncementShowCompleted')}}">
                    show completed
                  </a>
                </div>
            </div>
            <table class="table ">
                <thead>
                    <tr>
                        <th>Announcement Owner</th>
                        <th>Announcement Creator</th>
                        <th>Channel Name</th>
                        <th>Start Time Announcement</th>
                        <th>END time Announcement</th>
                        <th>Announcement Description</th>
                        <th>Announcement Note</th>

                    </tr>
                </thead>
                <tr>
                    @foreach($announcements as $announcement) @if($announcement->confirmed == 0)
                    <td>{{$announcement->announcements_owner}}</td>
                    <td>{{$announcement->user->name}}</td>
                    <td>{{$announcement->channel->name}}</td>
                    <td>{{date ('M,j,Y H:i', strtotime( $announcement->start_time))}}</td>
                    <td>{{date ('M,j,Y H:i', strtotime($announcement->end_time))}}</td>
                    <td>{{strip_tags($announcement->description)}}</td>
                    <td>{{$announcement->note}}</td>
                    <td>
                        <a href="{{url('employee/announcements/'.$announcement->id.'/edit')}}" class="btn btn-sm btn-primary">
                          edit
                        </a>
                    </td>

                </tr>
                @endif()
                @endforeach()

            </table>

            <div class="text-center">
                {!! $announcements->links() !!}
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="card">
            <div class="card-header">Announcement Create</div>
            <form class="form-group" method="post" action="{{route('EmployeeAnnouncemetCreate')}}">
                {{ csrf_field() }}
                <br>
                <label for="annoncement_owner">Annoncement owner</label>

                <select class="form-control selectpicker" name="announcements_owner">
                    <option>select annoucement owner</option>
                    @foreach($users as $user)

                    <label for="user">user <i class="fa fa-tv"></i> </label>
                    @if($user->role =='admin' or 'employee')
                    <option value="{{$user->name}}">{{$user->name}}</option>
                    @endif() @endforeach()

                </select>

                <label for="annoncement_owner">channel</label>
                <select class="form-control selectpicker" name="channel_id">
                    <option>select channel name</option>
                    @foreach($channels as $channel)

                    <label for="channel">channel <i class="fa fa-tv"></i> </label>
                    <option value="{{$channel->id}}">{{$channel->name}}</option>
                    @endforeach()

                </select>

                <label for="note"> note</label>
                <input type="text" class="form-control" name="note" placeholder="write note">
                <label for="description">decription</label>
                <textarea class="form-control" name="description" placeholder="write description" style="margin-top: 0px; margin-bottom: 0px; height: 129px;"></textarea>

                <label for="Start Time">Start Time:</label>
                <input class="form-control" type="date" name="start_time" id="today2" required="required" />

                <label for="End Time">End Time :</label>
                <input class="form-control" type="date" name="end_time" id="today2" required="required" />
                <br>
                <button type="submit" class="form-control btn btn-success">add</button>

            </form>

        </div>
    </div>
</div>
@endsection()
