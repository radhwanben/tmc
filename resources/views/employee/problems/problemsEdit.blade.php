@extends('layouts.app') @section('content')
<div class="row">
    @include('includes.admin-navbar')
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">CREATE NEW PROBLEM</div>
            <br>

            <form class="form-group" method="POST" action="{{route('empProblemUpdate',['id' =>$problem->id])}}">
                {{ csrf_field() }} {{ method_field('PUT') }}
                <div class="col-md-4 selectContainer">
                    <label for="chanel">Chanels <i class="fa fa-tv"></i> </label>
                    @if($problem->channel_id === $problem->channel->id)
                    <div class="form-group">
                        <select name="channel" class="form-control selectpicker">
                            <option value="{{$problem->channel->id}}">{{$problem->channel->name}}</option>
                            @foreach($channels as $chanel)
                            <option value="{{$chanel->id}}">{{$chanel->name}}</option>
                            @endforeach()
                        </select>
                        @endif()
                        <br> @include('includes.typeproblems')
                        <br>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="Start Time">Start Time:</label>
                                <input class="form-control" type="time" name="start_time" value="13:00" step="900" required="required" />

                            </div>

                            <div class="col-md-6 form-group">
                                <label for="End Time">End Time :</label>
                                <input class="form-control" type="time" name="end_time" value="13:10" step="900" required="required" />
                            </div>
                            <div class="col-md-6">
                                <label for="reason">Reason</label>
                                <input class="form-control" type="text" name="reason" required="required" value="{{$problem->reason}}">
                            </div>
                        </div>

                        <label for="action">What you do! : </label>
                        <input class="form-control" type="text" name="action" required="required" value="{{$problem->what_you_do}}">

                        <br>
                        <label for="description">Description <i class="fa fa-sticky-note"></i></label>

                    </div>
                    <div class="col-md-9">
                        <textarea rows="7" cols="70" class="form-control message" type="text" name="description" placeholder="description of error" required="required" value="{{$problem->description}}">{{$problem->description}}</textarea>
                    </div>

                    <br>
                    <div class="col-md-6">
                        <label for="action">Note : </label>
                        <input class="form-control" type="text" name="note" required="required" value="{{$problem->note}}">
                    </div>
                    <br>
                    <div class="col-md-8">
                        <button class="btn btn-success" type="submit">
                            <i class="fa fa-save"></i> save
                        </button>
                        <a class="btn btn-secondary" href="{{route('GetProblemsData')}}">
                            <i class="fa fa-home"></i> back
                        </a>
                    </div>
            </form>

            </div>

        </div>

    </div>
    @endsection