@extends('layouts.app')



@section('content')


<div class="row">
@include('includes.employee-navbar')

<div class="col-md-10">
	<div class="card">
<div class="card-header">PROBLEMS
		<a href="{{route('empProblemscreate')}}" class="btn btn-sm btn-success">add problems</a>
</div>
	<table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Channel Name</th>
                <th>Channel frequency</th>
                <th>Channel satelite</th>
                <th>problem start time</th>
                <th>problem end time</th>
                <th>problem reason</th>
                <th>problem description</th>
                <th>note</th>
                <th>user </th>
                <th>settings</th>
            </tr>
        </thead>
        @foreach($problems as $problem)
            @if(Auth::user()->id === $problem->user_id)

        <tr>
        <td>{{ $problem->channel->name }}</td>
        <td>{{ $problem->channel->frequency }}</td>
        <td>{{ $problem->channel->satelite }}</td>
        <td>{{ $problem->start_time }}</td>
        <td>{{ $problem->end_time }}</td>
        <td>{{ $problem->reason }}</td>   
        <td>{{ $problem->description }}</td>   
        <td>{{ $problem->note }}</td>
        <td>{{ $problem->user->name}}</td>
        <td><a href="{{url('admin/problems/'.$problem->id.'/edit')}}" class="btn btn-sm btn-primary">Edit</a></td>
        <td>
                        <form action="{{route('ProblemDelete',['id'=>$problem->id])}}" method="POST">
                    {{ csrf_field() }}
                {{ method_field('DELETE') }}  
              <input  class="btn btn-danger btn-sm pull-right" value="delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');">
            </form>

        </td>

    </tr>
        @endif()
        @endforeach()
    </table>




</div>



@endsection()