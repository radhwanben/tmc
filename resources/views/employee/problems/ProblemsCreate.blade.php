@extends('layouts.app') @section('content')
<div class="row">
    @include('includes.employee-navbar')
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">CREATE NEW PROBLEM</div>
            <br>

            <form class="form-group" action="{{route('empProblemsSave')}}" method="post" novalidate>
                {{ csrf_field() }}
                <div class="col-md-4 selectContainer">
                    <label for="chanel">Chanels <i class="fa fa-tv"></i> </label>
                    <select class="form-control selectpicker" name="channel">
                        @foreach($channels as $chanel)
                        <option value="{{$chanel->id}}">{{$chanel->name}}</option>
                        @endforeach()
                    </select>
                    <br> @include('includes.typeproblems')
                    <br>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="Start Time">Start Time:</label>
                            <input class="form-control" type="time" name="start_time" value="13:00" step="900" required="required" />

                        </div>

                        <div class="col-md-6 form-group">
                            <label for="End Time">End Time :</label>
                            <input class="form-control" type="time" name="end_time" value="13:10" step="900" required="required" />
                        </div>
                        <div class="col-md-6">
                            <label for="reason">Reason</label>
                            <input class="form-control" type="text" name="reason" required="required">
                        </div>
                    </div>
                    <label for="action">What you do! : </label>
                    <input class="form-control" type="text" name="action" required="required">

                    <br>
                    <label for="description">Description <i class="fa fa-sticky-note"></i></label>

                </div>
                <div class="col-md-8">
                    <textarea rows="7" cols="70" class="form-control message" type="text" name="description" placeholder="description of error" required="required"></textarea>
                </div>

                <br>
                <div class="col-md-6">
                    <label for="action">Note : </label>
                    <input class="form-control" type="text" name="note" required="required">
                </div>
                <br>
                <div class="col-md-8">
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-save"></i> save
                    </button>
                    <a class="btn btn-secondary" href="{{route('empGetProblemsData')}}">
                        <i class="fa fa-home"></i> back
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection