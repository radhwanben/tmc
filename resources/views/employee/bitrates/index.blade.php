@extends('layouts.app')



@section('content')


<div class="row">
@include('includes.employee-navbar')


<div class="col-md-8">
	<div class="card">
<div class="card-header">bitrates
		<a href="{{route('EmpBitrateCreate')}}" class="btn btn-sm btn-success">Add Bitrate</a>
</div>

	<table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Channel Name</th>
                <th>Channel frequency</th>
                <th>Bitrate Value</th>
                <th>Default Bitrate</th>
            </tr>
        </thead>   
            @foreach($bitrates as $bitrate)
            @if(Auth::user()->id === $bitrate->id_user)
            
        <tr>

            <td>{{$bitrate->channel->name}}</td>
            <td>{{$bitrate->channel->frequency}}</td>
            <td>{{$bitrate->bitrate_value}}</td>
            <td>{{$bitrate->default_bitrate}}</td>
            <td><a href="{{url('employee/bitrates/'.$bitrate->id.'/edit')}}" class="btn btn-sm btn-primary">Edit</a></td>
        </tr>
            @if($bitrate->bitrate_value >$bitrate->channel->bitrate_conparation)
            <div class="col-md-8">
            <p class="erorr">please check bitrate agian for {{$bitrate->channel->name}} channel </p>
            </div>
            @endif()
            @endif()
            @endforeach()

    </table>




</div>



</div>
<div class="col-md-2">
     <div class="card">
            <div class="card-header">CREATE NEW BITRATE</div>
            <br>

            <form class="form-group" action="{{route('empBitrateSave')}}" method="post">
                {{ csrf_field() }}
                    <label for="chanel">Chanels <i class="fa fa-tv"></i> </label>
                    <select class="form-control selectpicker" name="channel_id">
                        @foreach($channels as $channel)
                        <option value="{{$channel->id}}">{{$channel->name}}</option>
                        @endforeach()
                    </select>

                    <br>
                    <br>
                    <label for="Start Time">Bitrate Value:</label>
                    <input class="form-control" type="text" name="bitrate_value"  required="required" />

                    <button class="btn btn-success" type="submit">
                    <i class="fa fa-save"></i>
                    save
                    </button>
            </form>
        </div>
</div>


@endsection()