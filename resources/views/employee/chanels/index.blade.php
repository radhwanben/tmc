@extends('layouts.app')



@section('content')


<div class="row">
@include('includes.employee-navbar')


<div class="col-md-10">
	<div class="card">
<div class="card-header">CHANNELS
		<a href="{{route('EmpChannelCreate')}}" class="btn btn-sm btn-success">add channel</a>
</div>
	<table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Channel logo</th>
                <th>Channel Name</th>
                <th>Channel frequency</th>
                <th>Channel satelite</th>
                <th>Polarity</th>
                <th>bandwith</th>
                <th>start time monotoring</th>
                <th>bitrate conparation</th>
                <th>channel nationalite</th>
                <th>teleport</th>
                <th>provider</th>


            </tr>
        </thead>
        <tr>
            @foreach($channels as $channel)
                @if($channel->is_active == 1)
            <td>
                <a href="{{url('employee/channels/'.$channel->id)}}">
            <img class="rounded-circle" src="/storage/{{$channel->logo}}" />
                </a>
            </td>
            <td>{{$channel->name}}</td>
            <td>{{$channel->frequency}}</td>
            <td>{{$channel->satelite}}</td>
            <td>{{$channel->polarity}}</td>
            <td>{{$channel->bandwith}}</td>
            <td>{{$channel->start_time_monotoring}}</td>
            <td>{{$channel->bitrate_conparation}}</td>
            <td>{{$channel->nationalite}}</td>
            <td>
                @foreach($teleports as $teleport)
                @if($teleport->id == $channel->teleport_id )
                {{$teleport->name}}
                @endif()
                @endforeach()
            </td>
            <td>
                @foreach($providers as $providers)
                @if($provider->id == $channel->provider )
                {{$provider->name}}
                @endif()
                @endforeach()
            </td>


        </td>
                <td>
                <a href="{{url('employee/channels/'.$channel->id.'/edit')}}" class="btn btn-sm btn-primary">edit</a>
                </td>
                <td>
            <form action="{{route('empChannelDelete',['id'=>$channel->id])}}" method="POST">
                    {{ csrf_field() }}
                {{ method_field('DELETE') }}  
              <input  class="btn btn-danger btn-sm pull-right" value="delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');">
            </form>
            </td>
            
        </tr>
                @endif()

        @endforeach()

    </table>



</div>



@endsection()