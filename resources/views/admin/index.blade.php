@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.admin-navbar') 


    
    <br>

    <div class="col-md-8">
        <div class="card">
            <div class="card-header">ANNOUNCEMENTS
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Announcement Owner</th>
                        <th>Announcement Creator</th>
                        <th>Channel Name</th>
                        <th>Start Time Announcement</th>
                        <th>END time Announcement</th>
                        <th>Announcement Description</th>
                        <th>Announcement Note</th>

                    </tr>
                </thead>
                <tr>
                    @foreach($announcements as $announcement)
                    @if($announcement->confirmed == 0)
                    <td>{{$announcement->announcements_owner}}</td>
                    <td>{{optional($announcement->user)->name}}</td>
                    <td>{{optional($announcement->channel)->name}}</td>
                    <td>{{$announcement->start_time}}</td>
                    <td>{{$announcement->end_time}}</td>
                    <td>{{strip_tags($announcement->description)}}</td>
                    <td>{{$announcement->note}}</td>
                    </td>

                </tr>
                @endif()
                @endforeach()

            </table>

            <div class="text-center">
                {!! $announcements->links() !!}
            </div>

        </div>


    </div>

        <div class="col-md-2">
        <div class="card">
            <div class="card-header">
                Date:
            </div>
            <div class="text-center">
                {{Carbon\Carbon::now()->toDayDateTimeString()}}
            </div>
        </div>
    </div>
</div>
   @endsection()