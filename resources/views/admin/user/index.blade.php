@extends('layouts.app')



@section('content')


<div class="row">
@include('includes.admin-navbar')

<div class="col-md-8">
	<div class="card">
<div class="card-header">USERS
		<a href="{{route('UserCreate')}}" class="btn btn-sm btn-success">Add User</a>
</div>
	<table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>ID User</th>
                <th>User Name</th>
                <th>User Email</th>
                <th>User Role</th>
                <th>Last Login</th>
                <th>Last Ip Login</th>
            </tr>
        </thead>
        @foreach($users as $user)
        <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->role }}</td>
        <td>{{ $user->last_login_at}}</td>
        <td>{{ $user->last_login_ip }}</td>   
        <td><a href="{{url('admin/users/'.$user->id.'/edit')}}" class="btn btn-sm btn-primary">Edit</a></td>
        <td>
                        <form action="{{route('UsersDelete',['id'=>$user->id])}}" method="POST">
                    {{ csrf_field() }}
                {{ method_field('DELETE') }}  
              <input  class="btn btn-danger btn-sm pull-right" value="delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');">
            </form>

        </td>

    </tr>
        @endforeach()
    </table>




</div>



@endsection()