@extends('layouts.app') @section('content')
<div class="row">
    @include('includes.admin-navbar')

    <div class="col-md-9">
        <div class="card">
            <div class="card-header">channels informations</div>
            <table class="table">
                <thead>
                    <th>channel name</th>
                    <th>channel frequency</th>
                    <th>channel satelite</th>
                    <th>channel polarity</th>
                    <th>channel bandwith</th>
                    <th>channel start time monotoring</th>
                    <th>bitrate comparation</th>
                    <th>channel nationalite</th>
                    <th>teleport</th>
                    <th>provider</th>
                    <th>stauts</th>
                </thead>
                <tr>
                    <td>{{$channel->name}}</td>
                    <td>{{$channel->frequency}}</td>
                    <td>{{$channel->satelite}}</td>
                    <td>{{$channel->polarity}}</td>
                    <td>{{$channel->bandwith}}</td>
                    <td>{{date ('M,j,Y H:i', strtotime($channel->start_time_monotoring))}}</td>
                    <td>{{$channel->bitrate_conparation}}</td>
                    <td>{{$channel->nationalite}}</td>
                    <td>
                        @foreach($teleports as $teleport) @if($teleport->id == $channel->teleport_id ) {{$teleport->name}} @endif() @endforeach()

                    </td>
                    <td>
                        @foreach($providers as $provider) @if($provider->id == $channel->provider_id ) {{$provider->name}} @endif() @endforeach()
                    </td>
                    <td>
                        @if($channel->is_active == 1)
                        <p style="background-color: green ; color: white">enabled</p>
                        @else
                        <p style="background-color: red ; color: white">disabled</p>
                        @endif()
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="col-md-2"></div>
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">channels Contacts</div>
            <table class="table table-bordered">
                <thead>
                    <th>name</th>
                    <th>email</th>
                    <th>mobile</th>
                    <th>other</th>
                    <th>job title</th>
                    <th>notes</th>
                    <th>language</th>
                </thead>
                <tr>
                    @foreach($contacts as $contact) @if($contact->channel_id == $channel->id)
                    <td>{{$contact->name}}</td>
                    <td>{{$contact->email}}</td>
                    <td>{{$contact->mobile}}</td>
                    <td>{{$contact->other}}</td>
                    <td>{{$contact->job_title}}</td>
                    <td>{{$contact->notes}}</td>
                    <td>{{$contact->language}}</td>
                </tr>
                @endif() @endforeach()
            </table>
        </div>
				<br>
				<br>

    </div>

    <div class="col-md-2">
        <br>
    </div>

    <div class="col-md-9">
        <div class="card-header">list of announcements</div>
        @foreach($announcements as $announcement) @if($announcement->channel_id == $channel->id && $announcement->confirmed == 0)
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Announcement Owner</th>
                    <th>Announcement Creator</th>
                    <th>Channel Name</th>
                    <th>Start Time Announcement</th>
                    <th>END time Announcement</th>
                    <th>Announcement Description</th>
                    <th>Announcement Note</th>

                </tr>
            </thead>
            <tr>
                <td>{{$announcement->announcements_owner}}</td>
                <td>{{$announcement->user->name}}</td>
                <td>{{$announcement->channel->name}}</td>
                <td>{{$announcement->start_time}}</td>
                <td>{{$announcement->end_time}}</td>
                <td>{{strip_tags($announcement->description)}}</td>
                <td>{{$announcement->note}}</td>
            </tr>
            @endif() @endforeach()
        </table>

    </div>

</div>


@endsection()
