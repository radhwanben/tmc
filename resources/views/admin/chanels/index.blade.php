@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.admin-navbar')

    <div class="col-md-10">
        <div class="card">
            <div class="card-header">CHANNELS
                <a href="{{route('ChannelCreate')}}" class="btn btn-sm btn-success">add channel</a>
            </div>
            <table class="table table-bordered" >
                <thead>
                    <tr>
                        <th>Channel Logo</th>
                        <th>Channel Name</th>
                        <th>Channel frequency</th>
                        <th>Channel satelite</th>
                        <th>Polarity</th>
                        <th>bandwith</th>
                        <th>start time monotoring</th>
                        <th>bitrate conparation</th>
                        <th>channel nationalite</th>
                        <th>teleport</th>
                        <th>provider</th>
                    </tr>
                </thead>
                <tr>
                    @foreach($channels as $channel)
                    <td>
                        <a href="{{url('admin/channels/'.$channel->id)}}">
            							<img class="rounded-circle" src="/storage/{{$channel->logo}}" />
												</a>
                    </td>
                    <td>{{$channel->name}}</td>
                    <td>{{$channel->frequency}}</td>
                    <td>{{$channel->satelite}}</td>
                    <td>{{$channel->polarity}}</td>
                    <td>{{$channel->bandwith}}</td>
                    <td>{{date ('M,j,Y H:i', strtotime($channel->start_time_monotoring))}}</td>
                    <td>{{$channel->bitrate_conparation}}</td>
                    <td>{{$channel->nationalite}}</td>
                    <td>
                        @foreach($teleports as $teleport) @if($teleport->id == $channel->teleport_id ) {{$teleport->name}} @endif() @endforeach()
                    </td>
                    <td>
                        @foreach($providers as $provider)
                        @if($provider->id == $channel->provider_id )
                        {{$provider->name}}
                        @endif()
                        @endforeach()
                    </td>

                    @if($channel->is_active == 1)
                    <td style="background-color: green ; color: white">enabled</td>
                    @else
                    <td style="background-color: red ; color: white">disabled</td>
                    @endif()

                    <td>
                        <a href="{{url('admin/channels/'.$channel->id.'/edit')}}" class="btn btn-sm btn-primary">edit</a>
                    </td>


                </tr>
                @endforeach()

            </table>

            <div class="text-center">
                {!! $channels->links() !!}
            </div>

			</div>
  </div>
</div>
        @endsection()
