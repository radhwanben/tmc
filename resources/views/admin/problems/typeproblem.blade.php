@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.admin-navbar')

    <div class="col-md-5">
        <div class="card">
            <div class="card-header">
            PROBELM TYPE
            </div>
            <table class="table ">
                <thead>
                    <tr>
                        <th>Problem Type</th>
                    </tr>
                </thead>
                <tr>
                    @foreach($types as $type)
                    <td>{{$type->name}}
                        <a href="{{url('admin/problemstypes/'.$type->id.'/edit')}}" class="btn btn-sm btn-warning">edit</a>

                        
                    </td>
                  
                </tr>
                @endforeach()
            </table>
        </div>

    </div>
    
    <div class="col-md-2">
        <div class="card">
            <div class="card-header">Probelm Type Create</div>
            <form class="form-group" method="post" action="{{route('ProblemsTypeSave')}}">
                {{ csrf_field() }}
                <br>
                <label for="annoncement_owner">Type name</label>

                <input type="text" class="form-control" name="name" placeholder="write type">
                
                <br>
                <button type="submit" class="form-control btn btn-success">add</button>

            </form>

        </div>
    </div>


    <div class="col-md-3">
        <div class="card">
            <div class="card-header">Probelm Sub Type Create</div>
            <form class="form-group" method="post" action="{{route('ProblemsubTypeSave')}}">
                {{ csrf_field() }}
                <br>
                <select class="form-control selectpicker" name="type">
                    <option>select Probelm Type</option>
                    @foreach($types as $type)

                    <label for="type">type <i class="fa fa-tv"></i> </label>           
                    <option value="{{$type->id}}">{{$type->name}}</option>
                    @endforeach
                </select>
<br>

                <label for="annoncement_owner">Type name</label>

                <input type="text" class="form-control" name="name" placeholder="write type">
                
                <br>
                <button type="submit" class="form-control btn btn-primary">add</button>

            </form>

        </div>
    </div>

    <div class="col-md-8">
        <table class="table ">
                <thead>
                    <th>PROBLEM SUBTYPE</th>
                    <td>Type name</td>
                    <tr>
                    @foreach($subtypes as $subtype)
                    <td>{{$subtype->name}}
                        <a href="{{url('admin/problemsubtypes/'.$subtype->id.'/edit')}}" class="btn btn-sm btn-warning">edit</a>

                    </td>
                    @if($type->id == $subtype->id_category)
                    <td>{{$type->name}}</td>
                    @endif()

                </tr>

                    @endforeach()
    </div>


</div>


@endsection()