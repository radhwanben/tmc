@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.admin-navbar')

    <div class="col-md-10">
        <div class="card">
            <div class="card-header">PROBLEMS
                <a href="{{route('Problemscreate')}}" class="btn btn-sm btn-success">add problems</a>
                <a href="{{route('export')}}" class="btn btn-sm btn-primary">Export Excel</a>

            </div>
            <table class="table table-bordered" id="users-table">
                <thead>
                    <tr>
                        <th>Channel logo</th>
                        <th>Channel Name</th>
                        <th>Channel frequency</th>
                        <th>Channel satelite</th>
                        <th>problem start time</th>
                        <th>problem end time</th>
                        <th>problem reason</th>
                        <th>problem description</th>
                        <th>note</th>
                        <th>user </th>
                        <th>settings</th>
                    </tr>
                </thead>
                @foreach($problems as $problem)
                <tr>
                    <td>
                        <a href="{{url('admin/channels/'.$problem->channel->id)}}">
                    <img class="rounded-circle" src="/storage/{{$problem->channel->logo}}" />
                        </a>
                    </td>
                    <td>{{ $problem->channel->name }}</td>
                    <td>{{ $problem->channel->frequency }}</td>
                    <td>{{ $problem->channel->satelite }}</td>
                    <td>{{ $problem->start_time }}</td>
                    <td>{{ $problem->end_time }}</td>
                    <td>{{ $problem->reason }}</td>
                    <td>{{ strip_tags($problem->description) }}</td>
                    <td>{{ $problem->note }}</td>
                    <td>{{ $problem->user->name}}</td>
                    <td><a href="{{url('admin/problems/'.$problem->id.'/edit')}}" class="btn btn-sm btn-primary">Edit</a></td>
                    <td>
                        <form action="{{route('ProblemDelete',['id'=>$problem->id])}}" method="POST">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                            <input class="btn btn-danger btn-sm pull-right" value="delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');">
                        </form>

                    </td>

                </tr>
                @endforeach()
            </table>

        </div>
    </div>
</div>
        @endsection()