@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.admin-navbar')

    
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">Probelm Type Edit</div>
            <form class="form-group" method="post" action="{{route('ProblemstypesUpdate',['id' =>$types->id])}}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <br>
                <label for="annoncement_owner">Type name</label>

                <input type="text" class="form-control" name="name" value="{{$types->name}}">
                
                <br>
                <button type="submit" class="form-control btn btn-success">Edit</button>

            </form>
            <a href="" class="btn btn-sm btn-dark">back</a>

        </div>
    </div>
           <br>

            </form>


        <div class="col-md-2">
        <div class="card">
            <div class="card-header">
                Date:
            </div>
            <div class="text-center">
                {{Carbon\Carbon::now()->toDayDateTimeString()}}
        </div>
    </div>
        </div>
    
    </div>



</div>


@endsection()