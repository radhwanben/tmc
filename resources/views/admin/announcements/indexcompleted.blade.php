@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.admin-navbar')

    <div class="col-md-10">
        <div class="card">
            <div class="card-header">
            ANNOUNCEMENTS <div class="text-right"><a class="btn btn-sm btn-success" href="{{route('AnnouncementShow')}}">show discompleted </a></div>
            </div>
            <table class="table ">
                <thead>
                    <tr>
                        <th>Announcement Owner</th>
                        <th>Announcement Creator</th>
                        <th>Channel Name</th>
                        <th>Start Time Announcement</th>
                        <th>END time Announcement</th>
                        <th>Announcement Description</th>
                        <th>Announcement Note</th>

                    </tr>
                </thead>
                <tr>
                    @foreach($announcements as $announcement)

                    <td>{{$announcement->announcements_owner}}</td>
                    <td>{{$announcement->user->name}}</td>
                    <td>{{$announcement->channel->name}}</td>
                    <td>{{date ('M,j,Y H:i', strtotime( $announcement->start_time))}}</td>
                    <td>{{date ('M,j,Y H:i', strtotime($announcement->end_time))}}</td>
                    <td>{{strip_tags($announcement->description)}}</td>
                    <td>{{$announcement->note}}</td>
                    <td>
                        <a href="{{url('admin/announcements/'.$announcement->id.'/edit')}}" class="btn btn-sm btn-primary">edit</a>
                    </td>


                </tr>
                @endforeach()

            </table>


        </div>
    </div>


    </div>
</div>
        @endsection()
