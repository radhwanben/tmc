@extends('layouts.app')



@section('content')

<div class="row">
@include('includes.admin-navbar')


<div class="col-md-8">
	<div class="card">
	<div class="card-header">
		Edit Announcement
	</div>
	<div class="col-md-5">
	<form class="form-group" method="post" action="{{route('AnnouncementUpdate',['id' =>$announcement->id])}}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <label for="annoncement_owner">Annoncement owner</label>

        <select class="form-control selectpicker" name="announcements_owner">
            <option value="{{$announcement->user->name}}">{{$announcement->user->name}}</option>
            @foreach($users as $user)

            <label for="user">user <i class="fa fa-tv"></i> </label>
            @if($user->role =='admin' or 'employee')
            <option value="{{$user->name}}">{{$user->name}}</option>
            @endif() @endforeach()

        </select>

        <label for="annoncement_owner">channel</label>
        <select class="form-control selectpicker" name="channel_id">
            <option value="{{$announcement->channel->id}}">{{$announcement->channel->name}}</option>
            @foreach($channels as $channel)

            <label for="channel">channel <i class="fa fa-tv"></i> </label>
            <option value="{{$channel->id}}">{{$channel->name}}</option>
            @endforeach()

        </select>

        <label for="note"> note</label>
        <input type="text" class="form-control" name="note" placeholder="write note" value="{{$announcement->note}}">
        <label for="description">decription</label>
        <textarea class="form-control" name="description" placeholder="write description" style="margin-top: 0px; margin-bottom: 0px; height: 129px;">{{$announcement->description}}</textarea>
<br>
        <label for="Start Time">Start Time: </label>
        <input class="form-control" type="date" name="start_time" id="today2" value="{{$announcement->start_time}}" required="required" />
<br>
        <label for="End Time">End Time :{{$announcement->end_time}}</label>
        <input class="form-control" type="date" name="end_time" id="today2" required="required"  />
        <br>

           <form class="form-control">
                        <div class="form-group">
                          <label class="col-md-4 control-label">Status</label>
                          <select class="form-control" name="confirmed">
                            @if($announcement->confirmed == 1)
                            <option value="1">completed</option>
                            <option value="0">discompleted</option>

                            @else
                            <option value="0">discompleted</option>
                            <option value="1">completed</option>
                            @endif()
                          </select>
                        </div>
        <button type="submit" class="form-control btn btn-success">Edit</button>
        <br>
        <br>
        <a class="form-control btn btn-secondary" href="{{route('AnnouncementShow')}}">back</a>

    </form>
</div>
</div>


</div>




@endsection()
