@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.admin-navbar')

    <div class="col-md-10">
        <div class="card">
            <div class="card-header">CONTACT INFORMATION
                <a href="{{route('ContactinformationCreate')}}" class="btn btn-sm btn-success">add contact</a>
            </div>
            <table class="table table-bordered" id="users-table">
                <thead>
                    <tr>
                      
                        <th>channel logo</th>
                        <th>Channel name</th>
                        <th>Name</th>
                        <th>email</th>
                        <th>mobile </th>
                        <th>other</th>
                        <th>job_title</th>
                        <th>notes</th>
                        <th>language</th>
                    </tr>
                </thead>
                <tr>
                @foreach($contacts as $contact)

                <td>
                  <a href="{{url('admin/channels/'.$contact->channel->id)}}">
                    <img class="rounded-circle" src=" /storage/{{$contact->channel->logo}}" />
                  </a>
                </td>
                <td>{{$contact->channel->name}}</td>
                <td>{{$contact->name}}</td>
                <td>{{$contact->email}}</td>
                <td>{{$contact->mobile}}</td>
                <td>{{$contact->other}}</td>
                <td>{{$contact->job_title}}</td>
                <td>{{$contact->notes}}</td>
                <td>{{$contact->language}}</td>
</tr>
                @endforeach()

            </table>

        </div>
    </div>
</div>
        @endsection()
