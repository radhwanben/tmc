@extends('layouts.app') @section('content')
<div class="row">
    @include('includes.admin-navbar')
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Edit CONTACT CHANNEL</div>
            <br>

            <form class="form-group" action="{{route('ContactinformationSave')}}" method="post" novalidate>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="name">name:</label>
                        <input class="form-control" type="text" name="name" required="required" />

                    </div>

                    <div class="col-md-6 form-group">
                        <label for="email">email:</label>
                        <input class="form-control" type="email" name="email" required="required" />

                    </div>
                    <div class="col-md-6">
                        <label for="mobile">mobile</label>
                        <input class="form-control" type="text" name="mobile" required="required">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="other">other:</label>
                        <input class="form-control" type="text" name="other" required="required" />

                    </div>

                    <div class="col-md-6 form-group">
                        <label for="job_title">job title:</label>
                        <input class="form-control" type="text" name="job_title" required="required" />

                    </div>

                    <div class="col-md-6 form-group">
                        <label for="notes">notes:</label>
                        <input class="form-control" type="text" name="notes" required="required" />

                    </div>
                    <br>
                    <div class="col-md-6">
                        <label for="language">select language</label>
                        <select class="form-control selectpicker" name="language">
                            <option value="Afrikanns">Afrikanns</option>
                            <option value="Albanian">Albanian</option>
                            <option value="Arabic">Arabic</option>
                            <option value="Armenian">Armenian</option>
                            <option value="Basque">Basque</option>
                            <option value="Bengali">Bengali</option>
                            <option value="Bulgarian">Bulgarian</option>
                            <option value="Catalan">Catalan</option>
                            <option value="Cambodian">Cambodian</option>
                            <option value="Chinese (Mandarin)">Chinese (Mandarin)</option>
                            <option value="Croation">Croation</option>
                            <option value="Czech">Czech</option>
                            <option value="Danish">Danish</option>
                            <option value="Dutch">Dutch</option>
                            <option value="English">English</option>
                            <option value="Estonian">Estonian</option>
                            <option value="Fiji">Fiji</option>
                            <option value="Finnish">Finnish</option>
                            <option value="French">French</option>
                            <option value="Georgian">Georgian</option>
                            <option value="German">German</option>
                            <option value="Greek">Greek</option>
                            <option value="Gujarati">Gujarati</option>
                            <option value="Hebrew">Hebrew</option>
                            <option value="Hindi">Hindi</option>
                            <option value="Hungarian">Hungarian</option>
                            <option value="Icelandic">Icelandic</option>
                            <option value="Indonesian">Indonesian</option>
                            <option value="Irish">Irish</option>
                            <option value="Italian">Italian</option>
                            <option value="Japanese">Japanese</option>
                            <option value="Javanese">Javanese</option>
                            <option value="Korean">Korean</option>
                            <option value="Latin">Latin</option>
                            <option value="Latvian">Latvian</option>
                            <option value="Lithuanian">Lithuanian</option>
                            <option value="Macedonian">Macedonian</option>
                            <option value="Malay">Malay</option>
                            <option value="Malayalam">Malayalam</option>
                            <option value="Maltese">Maltese</option>
                            <option value="Maori">Maori</option>
                            <option value="Marathi">Marathi</option>
                            <option value="Mongolian">Mongolian</option>
                            <option value="Nepali">Nepali</option>
                            <option value="Norwegian">Norwegian</option>
                            <option value="Persian">Persian</option>
                            <option value="Polish">Polish</option>
                            <option value="Portuguese">Portuguese</option>
                            <option value="Punjabi">Punjabi</option>
                            <option value="Quechua">Quechua</option>
                            <option value="Romanian">Romanian</option>
                            <option value="Russian">Russian</option>
                            <option value="Samoan">Samoan</option>
                            <option value="Serbian">Serbian</option>
                            <option value="Slovak">Slovak</option>
                            <option value="Slovenian">Slovenian</option>
                            <option value="Spanish">Spanish</option>
                            <option value="Swahili">Swahili</option>
                            <option value="Swedish ">Swedish </option>
                            <option value="Tamil">Tamil</option>
                            <option value="Tatar">Tatar</option>
                            <option value="Telugu">Telugu</option>
                            <option value="Thai">Thai</option>
                            <option value="Tibetan">Tibetan</option>
                            <option value="Tonga">Tonga</option>
                            <option value="Turkish">Turkish</option>
                            <option value="Ukranian">Ukranian</option>
                            <option value="Urdu">Urdu</option>
                            <option value="Uzbek">Uzbek</option>
                            <option value="Vietnamese">Vietnamese</option>
                            <option value="Welsh">Welsh</option>
                            <option value="Xhosa">Xhosa</option>
                        </select>
                    </div>
                          <div class="col-md-6 selectContainer">
                    <label for="chanel">Chanels <i class="fa fa-tv"></i> </label>
                    <select class="form-control selectpicker" name="channel">
                        @foreach($channels as $chanel)
                        <option value="{{$chanel->id}}">{{$chanel->name}}</option>
                        @endforeach()
                    </select>
                </div>
                </div>

                <br>
                <br>
                <div class="col-md-8">
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-save"></i> save
                    </button>
                    <a class="btn btn-secondary" href="{{route('ContactinformationShow')}}">
                        <i class="fa fa-home"></i> back
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection