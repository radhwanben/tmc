@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.admin-navbar')

    <div class="col-md-8">
        <div class="card">
            <div class="card-header">bitrates
                <a href="{{route('BitrateCreate')}}" class="btn btn-sm btn-success">Add Bitrate</a>
                <div class="col-md-2">
                    <a href=""></a>
                </div>
            </div>

            <table class="table table-bordered" id="users-table">
                <thead>
                    <tr>
                        <th>Channel logo</th>
                        <th>Channel Name</th>
                        <th>Channel frequency</th>
                        <th>Bitrate Value</th>
                        <th>Default Bitrate</th>
                        <th>User Name</th>
                        <th>created at</th>
                    </tr>
                </thead>
                @foreach($bitrates as $bitrate)
                <tr>
                    <td>
                        <a href="{{url('admin/channels/'.optional($bitrate->channel)->id)}}">
                        <img class="rounded-circle" src="/storage/{{optional($bitrate->channel)->logo}}" />
                    </td>
                    <td>{{optional($bitrate->channel)->name}}</td>
                    <td>{{optional($bitrate->channel)->frequency}}</td>
                    <td>{{$bitrate->bitrate_value}}</td>
                    <td>{{$bitrate->default_bitrate}}</td>
                    <td>
                        @foreach($users as $user)
                        @if($bitrate->id_user == $user->id)
                        {{$user->name}}
                        @endif()
                        @endforeach()
                    </td>
                    <td>{{$bitrate->created_at->toDayDateTimeString()}}</td>
                    <td><a href="{{url('admin/bitrates/'.$bitrate->id.'/edit')}}" class="btn btn-sm btn-primary">Edit</a></td>
                    <td><a href="" class="btn btn-sm btn-danger">Delete</a></td>
                </tr>

                <div class="text-center">
                    @if($bitrate->bitrate_value >optional($bitrate->channel)->bitrate_conparation)

                    <p class="erorr">please check bitrate agian for {{optional($bitrate->channel)->name}} channel </p>
                    @endif()
                </div>

                @endforeach()

            </table>

            <br>
            <div class="text-center">
                {!! $bitrates->links() !!}
            </div>

        </div>
    </div>

    <div class="col-md-2">
        <div class="card">
            <div class="card-header">CREATE NEW BITRATE</div>
            <br>

            <form class="form-group" action="{{route('BitrateSave')}}" method="post">
                {{ csrf_field() }}
                <label for="chanel">Chanels <i class="fa fa-tv"></i> </label>
                <select class="form-control selectpicker" name="channel_id">
                    @foreach($channels as $channel)
                    <option value="{{$channel->id}}">{{$channel->name}}</option>
                    @endforeach()
                </select>

                <br>
                <br>
                <label for="Start Time">Bitrate Value:</label>
                <input class="form-control" type="text" name="bitrate_value" required="required" />
                <br>
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-save"></i> save
                </button>
            </form>

        </div>
    </div>
    @endsection()