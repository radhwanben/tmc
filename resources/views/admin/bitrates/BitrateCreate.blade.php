@extends('layouts.app')



@section('content')


<div class="row">
@include('includes.admin-navbar')

<div class="col-md-8">
        <div class="card">
            <div class="card-header">CREATE NEW BITRATE</div>
            <br>

            <form class="form-group" action="{{route('BitrateSave')}}" method="post">
                {{ csrf_field() }}
                <div class="col-md-4 selectContainer">
                    <label for="chanel">Chanels <i class="fa fa-tv"></i> </label>
                    <select class="form-control selectpicker" name="channel_id">
                    	@foreach($channels as $channel)
                    	<option value="{{$channel->id}}">{{$channel->name}}</option>
                    	@endforeach()
					</select>

                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="Start Time">Bitrate Value:</label>
                            <input class="form-control" type="text" name="bitrate_value"  required="required" />

                        </div>


                <div class="col-md-8">
                    <button class="btn btn-success" type="submit">
					<i class="fa fa-save"></i>
					save
					</button>
                    <a class="btn btn-secondary" href="{{route('BitrateShow')}}">
					<i class="fa fa-home"></i> back
					</a>
                </div>



</div>











@endsection()
