<link rel="stylesheet" href="https://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css
"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<table cellpadding="0" cellspacing="0" border="0" class="dataTable" id="example">
    <thead>
        <tr>
            <th>Rendering engine</th>
            <th>Browser</th>
            <th>Platform(s)</th>
            <th>Engine version</th>
            <th>CSS grade</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Trident</td>
            <td>Internet
                 Explorer 4.0</td>
            <td>Win 95+</td>
            <td> 4</td>
            <td>X</td>
        </tr>
        <tr>
            <td>Trident</td>
            <td>Internet
                 Explorer 5.0</td>
            <td>Win 95+</td>
            <td>5</td>
            <td>C</td>
        </tr>
        <tr>
            <td>Trident</td>
            <td>Internet
                 Explorer 5.5</td>
            <td>Win 95+</td>
            <td>5.5</td>
            <td>A</td>
        </tr>
    </tbody>
</table>
<script type="text/javascript" 
src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">