@extends('layouts.app') @section('content')

<div class="row">
    @include('includes.admin-navbar')


    <div class="col-md-8">
        <div class="card">
            <div class="card-header">TELEPORTS
                <a href="{{route('ProviderCreate')}}" class="btn btn-sm btn-success">add provider</a>
            </div>
            <table class="table table-bordered" id="users-table">
                <thead>
                    <tr>
                        <th>Name</th>             
                        <th>location</th>
                        <th>contact </th>
                    </tr>
                </thead>
                <tr>
                    @foreach($providers as $provider)
                    <td>{{$provider->name}}</td>
                    <td>{{$provider->location}}</td>
                    <td>{{strip_tags($provider->contact)}}</td>
                    <td>
                        <a class="btn btn-sm btn-primary" href="{{url('admin/providers/'.$provider->id.'/edit')}}">Edit</a>
                    </td>
                    <td>
                    <form action="{{route('ProviderDelete',['id'=>$provider->id])}}" method="POST">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                            <input class="btn btn-danger btn-sm pull-right" value="delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');">
                        </form>
                    </td>
                </tr>
                    @endforeach()
            </table>

        </div>
    </div>


        <div class="col-md-2">
        <div class="card">
            <div class="card-header">
                Date:
            </div>
            <div class="text-center">
                {{Carbon\Carbon::now()->toDayDateTimeString()}}
        </div>
    </div>

</div>
        @endsection()