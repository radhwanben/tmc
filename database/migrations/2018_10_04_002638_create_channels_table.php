<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('frequency');
            $table->string('polarity');
            $table->string('satelite');
            $table->integer('bandwith');
            $table->date('start_time_monotoring');
            $table->float('bitrate_conparation');
            $table->string('nationalite');
            $table->string('logo')->default('tmc.png');
            $table->boolean('is_active')->default('1');
            $table->integer('teleport_id')->nullable();
            $table->integer('provider_id')->nullable();
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
    }
}
