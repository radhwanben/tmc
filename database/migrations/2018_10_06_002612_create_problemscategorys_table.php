<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProblemscategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('problemscategorys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('problemsubcategorys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('id_category')->unsigned();
            $table->foreign('id_category')->references('id')->on('problemscategorys');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('problemsubcategorys');
        Schema::dropIfExists('problemscategorys');
    }
}
