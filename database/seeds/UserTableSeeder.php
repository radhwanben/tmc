<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
  public function run()
  {

    $admin = new User();
    $admin->name = 'Admin Name';
    $admin->email = 'admin@email.com';
    $admin->password = bcrypt('secret');
    $admin->role='admin';
    $admin->save();

    $employee = new User();
    $employee->name = 'Employee Name';
    $employee->email = 'employee@email.com';
    $employee->password = bcrypt('secret');
    $employee->role='employee';
    $employee->save();

    $customer = new User();
    $customer->name = 'customer Name';
    $customer->email = 'customer@email.com';
    $customer->password = bcrypt('secret');
    $customer->role = 'customer';
    $customer->save();

  }
}