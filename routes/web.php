<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('welcome');
   });
});


Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');


Route::get('admin',[
	'uses' => 'AdminController@index',
	'as' =>'Dashbord'
]);



Route::get('admin/problems/categorys',[
	'uses' => 'AdminController@MyCategoys',
	'as' =>'MyCategoys'
]);


Route::post('select-ajax',[

	'uses'=>'AdminController@selectAjax',
	'as'=>'select-ajax'
]);



Route::get('admin/users',[
	'uses' => 'AdminController@GetUserData',
	'as' =>'UsersLists'
]);


Route::get('admin/users/create',[
	'uses' => 'AdminController@CreateUser',
	'as' =>'UserCreate'
]);

Route::post('admin/users/create',[
	'uses' => 'AdminController@StoreUsers',
	'as' =>'StoreUsers'
]);

Route::get('admin/users/{id}/edit',[
	'uses' => 'AdminController@EditUsers',
	'as' =>'UsersEdit'
]);

Route::put('admin/users/{id}/',[
	'uses' => 'AdminController@UserUpdate',
	'as' =>'UserUpdate'
]);

Route::delete('admin/users/{id}',[
	'uses' => 'AdminController@UsersDelete',
	'as' =>'UsersDelete'
]);


Route::get('admin/channels',[
	'uses' => 'AdminController@channelsIndex',
	'as' => 'GetChannelsData'
]);



Route::get('admin/channels/create',[
	'uses' => 'AdminController@channelsCreate',
	'as' => 'ChannelCreate'
]);


Route::post('admin/channels/create',[
	'uses' => 'AdminController@channelStore',
	'as' => 'ChannelStore'
]);

Route::get('admin/channels/{id}',[
	'uses' =>'AdminController@channelShow',
	'as' => 'channelShow'

]);


Route::get('admin/channels/{id}/edit',[
	'uses' => 'AdminController@channelsEdit',
	'as' => 'ChannelEdit'
]);

Route::put('admin/channels/{id}',[
	'uses' => 'AdminController@ChannelUpdate',
	'as' => 'ChannelUpdate'
]);

Route::delete('admin/channels/{id}',[
	'uses' => 'AdminController@ChannelDelete',
	'as' => 'ChannelDelete'
]);


Route::get('admin/problems/',[
	'uses' => 'AdminController@ProblemsIndex',
	'as' => 'GetProblemsData'
]);

Route::get('admin/problems/create',[
	'uses' => 'AdminController@ProblemsCreate',
	'as' => 'Problemscreate'
]);

Route::post('admin/problems/create',[
	'uses' => 'AdminController@ProblemsSave',
	'as' => 'ProblemsSave'
]);


Route::get('admin/problems/{id}/edit',[
	'uses' => 'AdminController@problemsEdit',
	'as' => 'problemsEdit'
]);

Route::put('admin/problems/{id}',[
	'uses' => 'AdminController@ProblemUpdate',
	'as' => 'ProblemUpdate'
]);


Route::delete('admin/problems/{id}',[
	'uses' => 'AdminController@ProblemDelete',
	'as' => 'ProblemDelete'
]);

Route::get('admin/announcements/',[
	'uses' => 'AdminController@AnnouncementShow',
	'as' => 'AnnouncementShow'
]);

Route::post('admin/announcements/',[
	'uses' => 'AdminController@AnnouncementCreate',
	'as' =>'AnnouncementCreate'
]);

Route::get('admin/announcements/completed',[
	'uses' => 'AdminController@AnnouncementShowCompleted',
	'as' => 'AnnouncementShowCompleted'
]);


Route::get('admin/announcements/{id}/edit',[
	'uses' => 'AdminController@AnnouncementsEdit',
	'as' => 'AnnouncementsEdit'
]);


Route::put('admin/announcements/{id}',[
	'uses' => 'AdminController@AnnouncementUpdate',
	'as' => 'AnnouncementUpdate'
]);



Route::delete('admin/announcements/{id}',[
	'uses' => 'AdminController@AnnouncemetDelete',
	'as' => 'AnnouncemetDelete'
]);

Route::get('admin/bitrates/',[
	'uses' => 'AdminController@BitrateShow',
	'as' => 'BitrateShow'
]);

Route::get('admin/bitrates/create',[
	'uses' => 'AdminController@BitrateCreate',
	'as' => 'BitrateCreate'
]);

Route::post('admin/bitrates/',[
	'uses' => 'AdminController@BitrateSave',
	'as' => 'BitrateSave'
]);


Route::get('admin/bitrates/{id}/edit',[
	'uses' => 'AdminController@BitrateEdit',
	'as' => 'BitrateEdit'
]);

Route::put('admin/bitrates/{id}',[
	'uses' => 'AdminController@BitrateUpdate',
	'as' => 'BitrateUpdate'
]);


Route::get('admin/problemstypes/',[
	'uses' => 'AdminController@ProblemsTypeShow',
	'as' => 'ProblemsTypeShow'
]);


Route::post('admin/problemstypes/',[
	'uses' => 'AdminController@ProblemsTypeCreate',
	'as' => 'ProblemsTypeSave'
]);

Route::post('admin/problemsubtypes/',[
	'uses' => 'AdminController@ProblemsSubTypeCreate',
	'as' => 'ProblemsubTypeSave'
]);


Route::get('admin/problemsubtypes/{id}/edit',[
	'uses' => 'AdminController@ProblemsubTypesEdit',
	'as' => 'ProblemsubTypesEdit'
]);


Route::put('admin/problemsubtypes/{id}/edit',[
	'uses' => 'AdminController@ProblemsubTypesUpdate',
	'as' => 'ProblemsubTypesUpdate'
]);


Route::get('admin/problemstypes/{id}/edit',[
	'uses' => 'AdminController@ProblemsTypeEdit',
	'as' => 'ProblemsTypeEdit'
]);


Route::put('admin/problemstypes/{id}/edit',
	[
	'uses' => 'AdminController@ProblemstypesUpdate',
	'as' =>'ProblemstypesUpdate'
]);

Route::delete('admin/problemstypes/{id}',[
	'uses' => 'AdminController@ProblemstypesDelete',
	'as' => 'ProblemstypesDelete'

]);

Route::get('admin/clients/',[
	'uses' => 'AdminController@ContactinformationShow',
	'as' => 'ContactinformationShow'
]);


Route::get('admin/clients/create',[
	'uses' => 'AdminController@ContactinformationCreate',
	'as' => 'ContactinformationCreate'
]);

Route::post('admin/clients/create',[
	'uses' => 'AdminController@ContactinformationSave',
	'as' => 'ContactinformationSave'
]);

Route::get('admin/clients/{id}/edit',[
	'uses' => 'AdminController@ContactinformationEdit',
	'as' => 'ContactinformationEdit'
]);



Route::get('admin/teleports/',[
	'uses' => 'AdminController@TeleportShow',
	'as' => 'TeleportShow'
]);


Route::get('admin/teleports/create',[
	'uses' => 'AdminController@TeleportCreate',
	'as' => 'TeleportCreate'
]);

Route::post('admin/teleports/create',[
	'uses' => 'AdminController@TeleportSave',
	'as' => 'TeleportSave'
]);

Route::get('admin/teleports/{id}/edit',[
	'uses' => 'AdminController@TeleportEdit',
	'as' => 'TeleportEdit'
]);

Route::put('admin/teleports/{id}/edit',[
	'uses' => 'AdminController@TeleportUpdate',
	'as' => 'TeleportUpdate'
]);


Route::delete('admin/teleports/{id}',[
	'uses' => 'AdminController@TeleportDelete',
	'as' => 'TeleportDelete'

]);



Route::get('admin/providers/',[
	'uses' => 'AdminController@providershow',
	'as' => 'providershow'
]);


Route::get('admin/providers/create',[
	'uses' => 'AdminController@ProviderCreate',
	'as' => 'ProviderCreate'
]);

Route::post('admin/providers/create',[
	'uses' => 'AdminController@providersave',
	'as' => 'providersave'
]);

Route::get('admin/providers/{id}/edit',[
	'uses' => 'AdminController@ProviderEdit',
	'as' => 'ProviderEdit'
]);

Route::put('admin/providers/{id}/edit',[
	'uses' => 'AdminController@ProviderUpdate',
	'as' => 'ProviderUpdate'
]);


Route::delete('admin/providers/{id}',[
	'uses' => 'AdminController@ProviderDelete',
	'as' => 'ProviderDelete'

]);

Route::get('admin/problems/export',[
	'uses' => 'AdminController@export',
	'as' => 'export'
]);

/* employee routes */








Route::get('employee',[
	'uses' => 'EmployeeController@Dashbord',
	'as' => 'EmployeeDashbord'
]);

Route::post('employee',[
	'uses' => 'EmployeeController@EmployeeAnnouncemetCreate',
	'as' =>'EmployeeAnnouncemetCreate'
]);




Route::get('employee/channels',[
	'uses' => 'EmployeeController@channelsIndex',
	'as' => 'EmployeeGetChannelsData'
]);

Route::get('employee/channels/create',[
	'uses' => 'EmployeeController@EmpchannelsCreate',
	'as' => 'EmpChannelCreate'
]);


Route::post('employee/channels/create',[
	'uses' => 'EmployeeController@EmpchannelStore',
	'as' => 'EmpChannelStore'
]);

Route::get('employee/channels/{id}/edit',[
	'uses' => 'EmployeeController@empchannelsEdit',
	'as' => 'empChannelEdit'
]);

Route::put('employee/channels/{id}',[
	'uses' => 'EmployeeController@empChannelUpdate',
	'as' => 'empChannelUpdate'
]);

Route::delete('employee/channels/{id}',[
	'uses' => 'EmployeeController@empChannelDelete',
	'as' => 'empChannelDelete'
]);


Route::get('/employee/channels/{id}',[
	'uses' =>'EmployeeController@empchannelsShow',
	'as' => 'empchannelsShow'

]);


Route::get('employee/bitrates/',[
	'uses' => 'EmployeeController@empBitrateShow',
	'as' => 'empBitrateShow'
]);

Route::get('employee/bitrates/create',[
	'uses' => 'EmployeeController@empBitrateCreate',
	'as' => 'EmpBitrateCreate'
]);

Route::post('employee/bitrates/',[
	'uses' => 'EmployeeController@empBitrateSave',
	'as' => 'empBitrateSave'
]);


Route::get('employee/bitrates/{id}/edit',[
	'uses' => 'EmployeeController@empBitrateEdit',
	'as' => 'empBitrateEdit'
]);

Route::put('employee/bitrates/{id}',[
	'uses' => 'EmployeeController@empBitrateUpdate',
	'as' => 'empBitrateUpdate'
]);


Route::get('employee/problems/',[
	'uses' => 'EmployeeController@empProblemsIndex',
	'as' => 'empGetProblemsData'
]);

Route::get('employee/problems/create',[
	'uses' => 'EmployeeController@empProblemsCreate',
	'as' => 'empProblemscreate'
]);

Route::post('employee/problems/create',[
	'uses' => 'EmployeeController@empProblemsSave',
	'as' => 'empProblemsSave'
]);


Route::get('employee/problems/{id}/edit',[
	'uses' => 'EmployeeController@empproblemsEdit',
	'as' => 'empproblemsEdit'
]);

Route::put('employee/problems/{id}',[
	'uses' => 'EmployeeController@empProblemUpdate',
	'as' => 'empProblemUpdate'
]);


Route::delete('employee/problems/{id}',[
	'uses' => 'EmployeeController@empProblemDelete',
	'as' => 'empProblemDelete'
]);

Route::get('employee/problems/categorys',[
	'uses' => 'EmployeeController@empMyCategoys',
	'as' =>'empMyCategoys'
]);

Route::get('employee/announcements/{id}/edit',[
	'uses' => 'EmployeeController@empAnnouncementsEdit',
	'as' => 'empAnnouncementsEdit'
]);


Route::put('employee/announcements/{id}',[
	'uses' => 'EmployeeController@empAnnouncementUpdate',
	'as' => 'empAnnouncementUpdate'
]);

Route::get('employee/announcements/',[
	'uses' => 'EmployeeController@empAnnouncementShow',
	'as' => 'empAnnouncementShow'
]);

Route::get('employee/announcements/completed',[
	'uses' => 'EmployeeController@empAnnouncementShowCompleted',
	'as' => 'empAnnouncementShowCompleted'
]);